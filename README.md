# Statistical Physics of Disordered Systems

In this project, we used cavity method (belief propagation algorithm) to make inference on networks and ultimately retrieve the relevant average properties of a physical model. 

## Description

We start with a 1D Ising chain, then we explored the connection between Random Matrix Theory and graphical models, and in the end, we studied the Anderson model, a simplified model for conduction by electrons in random lattices using single instance cavity method and population dynamics algorithm. The project is available on my github.

## Running the code

You can run the code cell by cell. Just keep in mind that some cells(like "3.4: cavity variances g = i/ω" and "3.6:IPR") take long time(around 3 hours) to run completely.
